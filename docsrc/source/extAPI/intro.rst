Writting Extensions
===================

One the key features of the AuTest system is the ability to extend the system with different abilities.
This allow making test for a given application look and feel more natural.
For example testing a proxy server may require setting up an origin server and various network call for many of the tests.
While a threading library may need the ability to test that API compiles and run in expected ways.
The goal extending AuTest is to allow the ability to customize the system to make test more desirable
to write and run for the different applications and domains.

At this time the system allow extending of..